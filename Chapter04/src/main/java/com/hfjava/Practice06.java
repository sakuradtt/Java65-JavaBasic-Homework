package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 10:00
 * @Copyright 大牛版板所有
 */
public class Practice06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String answer = "y";
        int num;
        while ("y".equalsIgnoreCase(answer)){
            System.out.print("请用户输入一个0到2之间的整数：");
            num = input.nextInt();
            if (num == 0){
                System.out.println("你出的是石头");
            }
            else if (num == 1){
                System.out.println("你出的是剪刀");
            }
            else if (num == 2){
                System.out.println("你出的是布");
            }
            else {
                System.out.println("你输入的数字无效");
            }
            System.out.print("是否继续出拳（y/n）:");
            answer = input.next();
        }
    }
}
