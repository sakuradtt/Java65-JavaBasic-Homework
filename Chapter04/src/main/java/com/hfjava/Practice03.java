package com.hfjava;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 10:00
 * @Copyright 大牛版板所有
 */
public class Practice03 {
    public static void main(String[] args) {
        System.out.println("FlipFlop游戏：");
        for (int i=1;i<=100;i++){
            if (i%3 == 0 && i%5 != 0){
                System.out.println("Flip");
            }
            else if (i%5 ==0 && i%3 != 0){
                System.out.println("Flop");
            }
            else if (i%3 == 0 && i%5 == 0){
                System.out.println("FlipFlop");
            }
            else {
                System.out.println(i);
            }
        }
    }
}
