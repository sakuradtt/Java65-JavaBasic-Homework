package com.hfjava;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 10:00
 * @Copyright 大牛版板所有
 */
public class Practice04 {
    public static void main(String[] args) {
        for (int i=100;i<1000;i++){
            int geWei = i % 10;
            int shiWei = i /10 % 10;
            int baiWei = i / 100;
            double num = Math.pow(geWei,3) + Math.pow(shiWei,3) + Math.pow(baiWei,3);
            if (i == num){
                System.out.println("100~1000之间的水仙花素有："+i);
            }
        }
    }
}
