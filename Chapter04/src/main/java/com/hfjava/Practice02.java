package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 10:00
 * @Copyright 大牛版板所有
 */
public class Practice02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double num = 0;
        double avg = 0;
        double sum = 0;
        for (int i = 1; i<= 5;i++){
            System.out.print("请输入周"+i+"的学习时间：");
            num = input.nextDouble();
            sum += num;
        }
        avg = sum/5;
        System.out.println("周一~周五学习平均时间为："+avg+"小时！");
    }
}
