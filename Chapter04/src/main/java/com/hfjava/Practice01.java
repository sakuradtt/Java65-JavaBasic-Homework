package com.hfjava;

import java.text.NumberFormat;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 10:00
 * @Copyright 大牛版板所有
 */
public class Practice01 {
    public static void main(String[] args) {
        double principal = 10000;
        double ratio = 0.003;
        for (int i = 1;i <= 5;i++){
            principal += principal*ratio;
        }
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        System.out.println("5年后获得的本金是："+nf.format(principal));
    }
}
