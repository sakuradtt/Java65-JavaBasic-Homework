package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 10:00
 * @Copyright 大牛版板所有
 */
public class Practice07 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String admin,password;
        for (int i=2;i>=0;i--){
            System.out.print("请输入用户名：");
            admin = input.next();
            System.out.print("请输入密码：");
            password = input.next();
            if ("admin".equals(admin) && "123456".equals(password)){
                System.out.println("登录成功，欢迎进入二次元系统");
                break;
            }
            else {
                if (i != 0){
                    System.out.println("用户名和密码错误");
                    System.out.println("你还有"+i+"次机会，请重新输入");
                }
                else {
                    System.out.println("你没有权限进入系统");
                }
            }
        }
    }
}
