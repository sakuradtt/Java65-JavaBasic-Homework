package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 10:00
 * @Copyright 大牛版板所有
 */
public class Practice08 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int random = (int)(Math.random()*100);
        int num=0;
        int counter=0;
        System.out.println(random);
        for (int i=1;i<=20;i++){
            System.out.print("请你随便猜一个数字：");
            num = input.nextInt();
            if (num == random){
                break;
            }
            else if (num > random){
                System.out.println("太大了");
            }
            else {
                System.out.println("太小了");
            }
            counter++;
        }
        if (counter==1){
            System.out.println("你太有才了！");
        }
        else if (counter>1 && counter<=6){
            System.out.println("这么快就猜出来了，很聪明！");
        }
        else if (counter>7){
            System.out.println("猜了半天才猜出来，小同志，尚需努力呀！");
        }
    }
}
