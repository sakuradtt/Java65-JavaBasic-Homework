package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 10:00
 * @Copyright 大牛版板所有
 */
public class Practice05 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num = 0;
        int max = 0;
        int min = 0;
        int i = 1;
        while (i != 0){
            System.out.print("请输入一个整数（输入0结束）：");
            num = input.nextInt();
            if (i == 1){
                min = num;
            }
            i++;
            if (num == 0){
                i = num;
            }
            else {
                if (num < min){
                    min = num;
                }
                if (num > max){
                    max = num;
                }
            }
        }
        System.out.println("最大值是："+max+"      最小值是："+min);
    }
}
