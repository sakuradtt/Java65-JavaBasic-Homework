package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:07
 * @Copyright 大牛版板所有
 */
public class Practice03 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("请输入一个整数：");
        int num = input.nextInt();
        double encrypt = (num*10+5)/2+3.1415926;
        int last = (int)encrypt;
        System.out.println("加密结果为："+last);
    }
}
