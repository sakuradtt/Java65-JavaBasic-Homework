package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:07
 * @Copyright 大牛版板所有
 */
public class Practice07 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("请输入一个4位整数：");
        int integer = input.nextInt();
        int geWei = (integer%10+5)%10;
        int shiWei = (integer/10%10+5)%10;
        int baiWei = (integer/100%10+5)%10;
        int qianWei = (integer/1000+5)%10;
        int last = geWei*1000+shiWei*100+baiWei*10+qianWei;
        System.out.println("加密后的数字为："+last);
    }
}
