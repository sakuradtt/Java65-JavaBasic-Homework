package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:07
 * @Copyright 大牛版板所有
 */
public class Practice04 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("请输入姓名：");
        String name = input.next();
        System.out.print("请输入性别：");
        String sex = input.next();
        System.out.print("请输入年龄：");
        int age = input.nextInt();
        System.out.println("你的名字叫"+name+",你的年龄是"+age+"岁，你的性别为"+sex);
    }
}
