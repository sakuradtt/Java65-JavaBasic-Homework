package com.hfjava;

import java.text.NumberFormat;
import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:07
 * @Copyright 大牛版板所有
 */
public class Practice05 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("请输入圆形的半径：");
        double r = input.nextDouble();
        double s = 3.1415926*r*r;
        double c = 2*3.1415926*r;
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        System.out.println("圆的半径为：R="+nf.format(r)+";\n圆的面积为：S="+nf.format(s)+";\n圆的周长为：C="+nf.format(c));
    }
}
