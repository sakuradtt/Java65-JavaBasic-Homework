package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:07
 * @Copyright 大牛版板所有
 */
public class Practice01 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("请输入基本工资：");
        double basePay = input.nextDouble();
        double priceAllo = basePay*0.4;
        double rentAllo = basePay*0.25;
        double realityPay = basePay + priceAllo + rentAllo;
        System.out.println("实发工资为："+realityPay);
    }
}
