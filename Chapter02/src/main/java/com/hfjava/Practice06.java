package com.hfjava;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:07
 * @Copyright 大牛版板所有
 */
public class Practice06 {
    public static void main(String[] args){
        int principal = 10000;
        double current1 = principal + principal*0.0035;
        double current2 = current1 + current1*0.0035;
        double periodical1 = principal + principal*0.015;
        double periodical2 = principal + principal*0.021*2;
        System.out.println("活期1年的本金总计为："+Math.round(current1)+";\n活期2年的本金总计为："+Math.round(current2)+
                "；\n定期1年的本金总计为："+Math.round(periodical1)+";\n定期2年的本金总计为："+Math.round(periodical2));
    }
}
