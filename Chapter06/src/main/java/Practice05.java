import java.util.Arrays;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice05
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/26 18:45
 * @Copyright 大牛版板所有
 */
public class Practice05 {
    public static void main(String[] args) {
        int[] numbers = new int[10];
        int num;
        for (int i = 0; i < numbers.length; i++) {
            num = (int)(100*Math.random());
            numbers[i] = num;
        }
        System.out.println("生成的数组是：numbers="+ Arrays.toString(numbers));
        int max,min;
        min = max = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > max){
                max = numbers[i];
            }
            if (numbers[i] < min){
                min = numbers[i];
            }
        }
        System.out.println("随机数组的最大值为："+max+"，最小值为："+min);
    }
}
