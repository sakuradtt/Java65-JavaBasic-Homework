import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/26 17:27
 * @Copyright 大牛版板所有
 */
public class Practice01 {
    public static void main(String[] args) {
        int[] mobilePrice = new int[4];
        System.out.println("请输入4家店的手机价格：");
        Scanner input = new Scanner(System.in);
        for (int i = 0; i <mobilePrice.length; i++) {
            System.out.print("第"+(i+1)+"家店的手机价格是：");
            mobilePrice[i] = input.nextInt();
        }
        int min = mobilePrice[0];
        for (int i = 1; i < mobilePrice.length; i++) {
            if (mobilePrice[i]<min){
                min = mobilePrice[i];
            }
        }
        System.out.println("4家店手机价格最低的是："+min);
    }
}
