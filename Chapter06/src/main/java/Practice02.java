import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice02
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/26 18:13
 * @Copyright 大牛版板所有
 */
public class Practice02 {
    public static void main(String[] args) {
        double[] moneys = new double[5];
        double sum = 0;
        Scanner input = new Scanner(System.in);
        System.out.println("请输入会员本月的消费记录");
        for (int i = 0; i < moneys.length; i++) {
            System.out.print("请输入第"+(i+1)+"笔购物金额：");
            moneys[i] = input.nextDouble();
        }

        System.out.println("\n序号\t\t\t金额（元）");
        for (int i = 0; i <moneys.length ; i++) {
            sum += moneys[i];
            System.out.println((i+1)+"\t\t\t\t"+moneys[i]);
        }
        System.out.println("总金额\t\t\t"+sum);
    }
}
