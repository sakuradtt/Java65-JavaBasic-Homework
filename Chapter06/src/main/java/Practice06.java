import java.util.Arrays;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice06
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/26 18:56
 * @Copyright 大牛版板所有
 */
public class Practice06 {
    public static void main(String[] args) {
        int[] oldArr={1,3,4,5,0,0,6,6,0,5,4,7,6,7,0,5};
        int num = oldArr.length;
        for (int i = 0; i < oldArr.length; i++) {
            if (oldArr[i] == 0){
                num--;
            }
        }
        System.out.println("新生成的数组长度为："+num);
        int[] newArr = new int[num];
        int count = 0;
        for (int i = 0; i < oldArr.length; i++) {
            if (oldArr[i] != 0){
                newArr[count] = oldArr[i];
                count++;
            }
        }
        System.out.println("新生成的数组为：newArr="+ Arrays.toString(newArr));
    }
}
