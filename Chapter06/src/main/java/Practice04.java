import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice04
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/26 18:31
 * @Copyright 大牛版板所有
 */
public class Practice04 {
    public static void main(String[] args) {
        String[] words = new String[10];
        boolean flag = true;
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < words.length; i++) {
            System.out.print("请输入第"+(i+1)+"个单词：");
            words[i] = input.next();
        }
        System.out.print("请输入一个新的单词：");
        String newWord = input.next();
        for (int i = 0;i <words.length;i++){
            if (newWord.equals(words[i])){
                flag = false;
                break;
            }
        }
        if (!flag){
            System.out.println("Yes");
        }
        else {
            System.out.println("No");
        }
    }
}
