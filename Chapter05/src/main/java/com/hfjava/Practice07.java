package com.hfjava;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 18:46
 * @Copyright 大牛版板所有
 */
public class Practice07 {
    public static void main(String[] args) {
        for (int i=1;i<=9;i++){
            for (int j=1;j<=i;j++){
                System.out.print(j+"x"+i+"="+(i*j)+"\t");
            }
            System.out.println(" ");
        }
    }
}
