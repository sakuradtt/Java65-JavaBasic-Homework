package com.hfjava;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 18:46
 * @Copyright 大牛版板所有
 */
public class Practice02 {
    public static void main(String[] args) {
        for (int i =100;i>=5;i -=5){
            System.out.print(i+" ");
        }
        //=========================================

        int j = 100;
        while (j>=5){
            System.out.print(j+" ");
            j -=5;
        }
        //=========================================

        int k = 100;
        do {
            System.out.print(k+" ");
            k -=5;
        }while (k>=5);
    }
}
