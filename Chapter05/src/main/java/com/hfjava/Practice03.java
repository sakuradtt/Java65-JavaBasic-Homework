package com.hfjava;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 18:46
 * @Copyright 大牛版板所有
 */
public class Practice03 {
    public static void main(String[] args) {
        int num = 8;
        for (int i =1;i<=num;i++){
            for (int j =num-i;j>=0;j--){
                System.out.print(" ");
            }
            for (int j =1;j<=2*i-1;j++){
                System.out.print(i);
            }
            System.out.println(" ");
        }
    }
}
