package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 18:46
 * @Copyright 大牛版板所有
 */
public class Practice04 {
    public static void main(String[] args) {
        int score = 0;
        int total = 0;
        int avg = 0;
        int max = 0;
        int min = 0;
        String className = "";
        Scanner input = new Scanner(System.in);
        for(int i=1;i<=3;i++){
            System.out.print("请输入班级名称：");
            className= input.next();
            System.out.println("输入"+className+"班级5个学生的成绩：");
            for (int j=1;j<=5;j++){
                System.out.print("请输入第"+j+"名学生的成绩：");
                score = input.nextInt();
                total += score;
                if (j==1){
                    min = score;
                }
                if (score<min){
                    min = score;
                }
                if (score>max){
                    max = score;
                }
            }
            avg = total/5;
            System.out.println(className+"班级的总分是："+total+"，平均分是："+avg+"，最高分是："+max+"，最低分是："+min);
        }
    }
}
