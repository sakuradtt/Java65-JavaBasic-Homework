package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 18:46
 * @Copyright 大牛版板所有
 */
public class Practice01 {
    public static void main(String[] args) {
        int num=1;
        while (num!=0){
            Scanner input = new Scanner(System.in);
            System.out.print("请输入一个1~7之间的整数：");
            if (input.hasNextInt()){
                num = input.nextInt();
                if (num!=0){
                    switch (num){
                        case 1:
                            System.out.println("星期一");
                            break;
                        case 2:
                            System.out.println("星期二");
                            break;
                        case 3:
                            System.out.println("星期三");
                            break;
                        case 4:
                            System.out.println("星期四");
                            break;
                        case 5:
                            System.out.println("星期五");
                            break;
                        case 6:
                            System.out.println("星期六");
                            break;
                        case 7:
                            System.out.println("星期日");
                            break;
                        default:
                            System.out.println("你输入的数字无效，只能输入1~7之间的数字，请重新输入：");
                    }
                }
                else {
                    System.out.println("程序结束！");
                }
            }
            else {
                System.out.println("你输入的不是数字，请重新输入：");
            }
        }
    }
}
