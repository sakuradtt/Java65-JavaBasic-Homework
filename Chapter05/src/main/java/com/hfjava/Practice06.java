package com.hfjava;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 18:46
 * @Copyright 大牛版板所有
 */
public class Practice06 {
    public static void main(String[] args) {
        int num = 5;
        // 打印矩形：
        System.out.println("矩形：");
        for (int i=1;i<=num;i++){
            for (int j=1;j<=num;j++){
                System.out.print("*");
            }
            System.out.println(" ");
        }

        // 打印直角三角形：
        System.out.println("直角三角形：");
        for (int i=1;i<=num;i++){
            for (int j=1;j<=i;j++){
                System.out.print("*");
            }
            System.out.println(" ");
        }

        // 打印等腰三角形：
        System.out.println("等腰三角形：");
        for (int i=1;i<=num;i++){
            for (int k=num-i;k>=0;k--){
                System.out.print(" ");
            }
            for (int j=1;j<=2*i-1;j++){
                System.out.print("*");
            }
            System.out.println(" ");
        }

        // 菱形：
        System.out.println("菱形：");
        for (int i=1;i<=num;i++){
            for (int k=num-i;k>=0;k--){
                System.out.print(" ");
            }
            for (int j=1;j<=2*i-1;j++){
                System.out.print("*");
            }
            System.out.println(" ");
        }
        for (int i=1;i<=num;i++){
            for (int k=0;k<=i;k++){
                System.out.print(" ");
            }
            for (int j=2*num-2;j>2*i-1;j--){
                System.out.print("*");
            }
            System.out.println(" ");
        }
    }
}
