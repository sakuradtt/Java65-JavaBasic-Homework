package com.hfjava;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: PACKAGE_NAME
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 18:46
 * @Copyright 大牛版板所有
 */
public class Practice05 {
    public static void main(String[] args) {
        boolean isPrime = true;
        System.out.println("1~100之间的质数有：");
        for (int i =2;i<100;i++){
            isPrime = true;
            for (int j=2;j<i;j++){
                if (i % j ==0){
                    isPrime = false;
                    break;
                }
            }
            if (isPrime){
                System.out.print(i+" ");
            }
        }
    }
}
