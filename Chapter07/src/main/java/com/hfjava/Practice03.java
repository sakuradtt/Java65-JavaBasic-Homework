package com.hfjava;

import java.util.Arrays;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice03
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/27 20:03
 * @Copyright 大牛版板所有
 */
public class Practice03 {
    public static void main(String[] args) {
        int[] arrs = {10,20,11,35,6,89};
        int tmp;
        for (int i = 0; i < arrs.length-1; i++) {
            for (int j = 0; j < arrs.length-1-i; j++) {
                if (arrs[j] < arrs[j+1]){
                    tmp = arrs[j+1];
                    arrs[j+1] = arrs[j];
                    arrs[j] = tmp;
                }
            }
        }
        System.out.println("arrs="+ Arrays.toString(arrs));
    }
}
