package com.hfjava;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice05
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/27 20:20
 * @Copyright 大牛版板所有
 */
public class Practice05 {
    public static void main(String[] args) {
        int[][] scores = new int[3][5];
        int random;
        int[] totals = new int[3];
        int[] avgs = new int[5];
        int[] sums = new int[5];
        // 循环生成每个学员的成绩
        for (int i = 0; i < scores.length; i++) {
            for (int j = 0; j < scores[i].length; j++) {
                random = (int)(Math.random()*100);
                scores[i][j] = random;
                totals[i] += scores[i][j];
                sums[j] += scores[i][j];
            }
        }
        System.out.println("学员\tJava\tJSP\tCoreJava\tMySQL\tHTML5");
        for (int i = 0; i < scores.length; i++) {
            System.out.print((i+1)+"\t\t");
            for (int j = 0; j < scores[i].length; j++) {
                System.out.print(scores[i][j]+"\t\t");
            }
            System.out.println("\n");
        }
        // 每个学员的总分
        for (int i = 0; i < totals.length; i++) {
            System.out.println("第"+(i+1)+"个学员的总分为：totals="+totals[i]);
        }
        // 所有学员某门课的平均成绩
        System.out.println("==============================================================\n");
        for (int i = 0; i < sums.length; i++) {
            avgs[i] = sums[i]/5;
            System.out.println("第"+(i+1)+"门课程的平均分为：avgs="+avgs[i]);
        }
    }
}
