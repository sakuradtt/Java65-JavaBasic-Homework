package com.hfjava;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice06
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/28 9:20
 * @Copyright 大牛版板所有
 */
public class Practice06 {
    public static void main(String[] args) {
        String[] fruits = new String[5];
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < fruits.length; i++) {
            System.out.print("请输入一个水果的英文单词：");
            fruits[i] = input.next();
        }
        Arrays.sort(fruits);
        System.out.println("这些水果的英文名在字典中出现的顺序为：");
        for (String frus:fruits) {
            System.out.println(frus);
        }

    }
}
