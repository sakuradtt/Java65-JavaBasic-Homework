package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice02
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/27 19:21
 * @Copyright 大牛版板所有
 */
public class Practice02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double[][] scores = new double[5][3];
        double[] totals = new double[5];
        double[] avgs = new double[5];
        double[] mins = new double[5];
        double[] maxs = new double[5];
        for (int i = 0; i <scores.length ; i++) {
            System.out.println("第"+(i+1)+"个学生的成绩：");
            for (int j = 0; j < scores[i].length; j++) {
                System.out.print("第"+(j+1)+"门成绩是：");
                scores[i][j] = input.nextDouble();
                totals[i] += scores[i][j];
                maxs[i] = scores[i][0];
                mins[i] = scores[i][0];
                if (maxs[i]<scores[i][j]) {
                    maxs[i] = scores[i][j];
                }
                if (mins[i]>scores[i][j]){
                    mins[i] = scores[i][j];
                }
            }
        }

        for (int i = 0; i < totals.length; i++) {
            avgs[i] = totals[i]/3;
            System.out.println("第"+(i+1)+"个学生的总分为："+totals[i]+"分，平均分为："+avgs[i]
            +"分，最低分为："+mins[i]+"分，最高分为："+maxs[i]);
        }
    }
}
