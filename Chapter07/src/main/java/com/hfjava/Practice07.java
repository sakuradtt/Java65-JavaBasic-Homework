package com.hfjava;

import java.util.Arrays;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice07
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/28 9:31
 * @Copyright 大牛版板所有
 */
public class Practice07 {
    public static void main(String[] args) {
        int[] arr11 = {1,2,3,4,5,6,7,8,9,0,3,2,4,5,6,7,4,32,2,1,1,4,6,3};
        int[] newArr = {1,2,3,4,5,6,7,8,9,0,3,2,4,5,6,7,4,32,2,1,1,4,6,3};
        int counter = 0;
        for (int i = 0; i <newArr.length ; i++) {
            if (newArr[i] == 0){
                counter++;
            }
        }
        System.out.println("counter="+counter);
        for (int i = 0; i < arr11.length; i++) {
            for (int j = i+1; j < arr11.length; j++) {
                if (arr11[i] == newArr[j]){
                    newArr[j] = 0;
                }
            }
        }
        Arrays.sort(newArr);
        System.out.println(Arrays.toString(newArr));
        System.out.println("===========================================================");
        int index = 0;
        for (int i = 0; i < newArr.length; i++) {
            if (newArr[i] != 0){
                index = i;
                break;
            }
        }
        int[] newNums = new int[newArr.length-index+counter];
        System.out.println(index);
        System.arraycopy(newArr,index-counter,newNums,0,newArr.length-index+counter);
        System.out.println(Arrays.toString(newNums));
    }
}
