package com.hfjava;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice04
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/27 20:12
 * @Copyright 大牛版板所有
 */
public class Practice04 {
    public static void main(String[] args) {
        int[][] sales = {{22,66,44},{77,33,88},{25,45,65},{11,66,99}};
        int[] totals = new int[4];
        for (int i = 0; i <sales.length ; i++) {
            for (int j = 0; j <sales[i].length ; j++) {
                totals[i] += sales[i][j];
            }
        }
        for (int i = 0; i <totals.length ; i++) {
            System.out.println("第"+(i+1)+"季度的总销售额是："+totals[i]);
        }
    }
}
