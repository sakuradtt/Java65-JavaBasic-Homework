package com.hfjava;

import java.util.Arrays;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/27 18:31
 * @Copyright 大牛版板所有
 */
public class Practice01 {
    public static void main(String[] args) {
        char[] chars = {'p','z','x','a','t','b','e'};
        Arrays.sort(chars);
        System.out.println("chars="+Arrays.toString(chars));
        int index = Arrays.binarySearch(chars,'e');
        System.out.println("word='e'的下标为："+index);
    }
}
