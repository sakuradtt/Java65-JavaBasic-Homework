package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:27
 * @Copyright 大牛版板所有
 */
public class Practice06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("请输入第一个数字：");
        int firstNumber = input.nextInt();
        System.out.print("请输入第二个数字：");
        int secondNumber = input.nextInt();
        System.out.print("请输入运算符（+ - * / %）：");
        char operator = input.next().charAt(0);
        int result = 0;
        switch (operator){
            case '+':
                result = firstNumber + secondNumber;
                break;
            case '-':
                result = firstNumber - secondNumber;
                break;
            case '*':
                result = firstNumber * secondNumber;
                break;
            case '/':
                if (secondNumber ==0){
                    throw new RuntimeException("除数不能为0");
                }
                else{
                    result = firstNumber / secondNumber;
                }
                break;
            case '%':
                result = firstNumber % secondNumber;
                break;
            default:
                System.out.println("你输入的运算符无效");
                break;
        }
        System.out.println(String.format("计算的结果为：%d %c %d = %d",firstNumber,
                operator,secondNumber,result));
    }
}
