package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:27
 * @Copyright 大牛版板所有
 */
public class Practice03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("请输入1-12之间的月份：");
        double price = 5000;
        double discountPr = 0;
        if (input.hasNextInt()){
            int month = input.nextInt();
            if (month>0 && month<13){
                System.out.print("你输入你选择的舱位(头等舱、商务舱、经济舱)：");
                String cabin = input.next();
                if (month>=4 && month<=10){
                    switch (cabin){
                        case "头等舱":
                            discountPr = price*0.9;
                            break;
                        case "商务舱":
                            discountPr = price*0.8;
                            break;
                        case "经济舱":
                            discountPr = price*0.7;
                            break;
                        default:
                            System.out.println("你输入的舱位无效");
                            break;
                    }
                }
                else {
                    switch (cabin){
                        case "头等舱":
                            discountPr = price*0.5;
                            break;
                        case "商务舱":
                            discountPr = price*0.4;
                            break;
                        case "经济舱":
                            discountPr = price*0.3;
                            break;
                        default:
                            System.out.println("你输入的舱位无效");
                            break;
                    }
                }
                System.out.println("实际的机票价格为："+discountPr);
            }
            else {
                System.out.println("你输入的月份无效");
            }
        }
        else {
            System.out.println("你输入的月份不是整数");
        }
    }
}
