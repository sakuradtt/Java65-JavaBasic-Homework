package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:27
 * @Copyright 大牛版板所有
 */
public class Practice02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("请输入英文单词的第一个字母：");
        char firstAlph = input.next().charAt(0);
        System.out.print("请输入英文单词的第二个字母：");
        char secondAlph = input.next().charAt(0);
        switch (firstAlph){
            case 'M':
                System.out.println("星期一");
                break;
            case 'T':
                if (secondAlph == 'u'){
                    System.out.println("星期二");
                }
                else if (secondAlph == 'h'){
                    System.out.println("星期四");
                }
                else {
                    System.out.println("你输入的第二个字母无效");
                }
                break;
            case 'W':
                System.out.println("星期三");
                break;
            case 'F':
                System.out.println("星期五");
                break;
            case 'S':
                if (secondAlph == 'a'){
                    System.out.println("星期六");
                }
                else if (secondAlph == 'u'){
                    System.out.println("星期日");
                }
                else {
                    System.out.println("你输入的第二个字母无效");
                }
                break;
            default:
                System.out.println("你输入的第一个字母无效");
                break;
        }
    }
}
