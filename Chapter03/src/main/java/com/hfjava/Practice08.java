package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:27
 * @Copyright 大牛版板所有
 */
public class Practice08 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("请输入小明的成绩：");
        double score = input.nextDouble();
        if (score == 100){
            System.out.println("小明想干啥就干啥");
        }
        else if (score>=90 && score<100){
            System.out.println("看电视、吃零食");
        }
        else if (score>=60 && score<90){
            System.out.println("看书、写作业");
        }
        else if (score>=0 && score<60){
            System.out.println("做家务、洗碗、拖地");
        }
        else {
            System.out.println("你输入的成绩无效");
        }
    }
}
