package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:27
 * @Copyright 大牛版板所有
 */
public class Practice07 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("请输入一个整数：");
        if (input.hasNextInt()){
            int num = input.nextInt();
            if (num % 2 ==0){
                System.out.println(num+"是一个偶数");
            }
            else {
                System.out.println(num+"是一个奇数");
            }
        }
        else {
            System.out.println("你输入的数字不是整数");
        }
    }
}
