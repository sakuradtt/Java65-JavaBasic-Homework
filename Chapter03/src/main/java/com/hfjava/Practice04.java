package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:27
 * @Copyright 大牛版板所有
 */
public class Practice04 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("请输入当前月份：");
        if (input.hasNextInt()) {
            int month = input.nextInt();
            if (month>0 && month<13){
                switch (month){
                    case 3:
                    case 4:
                    case 5:
                        System.out.println("春季");
                        break;
                    case 6:
                    case 7:
                    case 8:
                        System.out.println("夏季");
                        break;
                    case 9:
                    case 10:
                    case 11:
                        System.out.println("秋季");
                        break;
                    case 12:
                    case 1:
                    case 2:
                        System.out.println("冬季");
                        break;
                    default:
                        System.out.println("你输入的月份无效");
                }
            }
            else {
                System.out.println("你输入的月份无效");
            }
        }
        else {
            System.out.println("你输入的月份不是整数");
        }
    }
}
