package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:27
 * @Copyright 大牛版板所有
 */
public class Practice05 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("请输入第一个整数：");
        int firstNum = input.nextInt();
        System.out.print("请输入第二个整数：");
        int secondNum = input.nextInt();
        System.out.print("请输入第三个整数：");
        int thirdNum = input.nextInt();
        int max = 0;
        int min = 0;
        if (firstNum >= secondNum){
            if (secondNum <= thirdNum){
                min = secondNum;
                if (firstNum >= thirdNum){
                    max = firstNum;
                }
                else {
                    max = thirdNum;
                }
            }
            else {
                min = thirdNum;
                max = firstNum;
            }
        }
        else {
            if (secondNum <= thirdNum){
                min = firstNum;
                max = thirdNum;
            }
            else {
                max = secondNum;
                if (firstNum <= thirdNum){
                    min = firstNum;
                }
                else {
                    min = thirdNum;
                }
            }
        }
        System.out.println("三个数的最大值为："+max+"；最小值为："+min);
    }
}
