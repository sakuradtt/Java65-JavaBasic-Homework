package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:27
 * @Copyright 大牛版板所有
 */
public class Practice01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("请输入小明的考试成绩：");
        if (input.hasNextInt()){
            int score = input.nextInt();
            if (score>90){
                System.out.println("爸爸带小明去欧洲旅行");
            }
            else {
                System.out.println("在家努力学习");
            }
        }
        else {
            System.out.println("你输入的成绩无效");
        }
    }
}
