package com.hfjava;

import java.util.Scanner;

/**
 * @author Administrator
 * @version V1.0
 * @ProjectName:IntelliJ IDEA
 * @PackageName: com.hfjava
 * @ClassName: Practice01
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2019/11/25 9:27
 * @Copyright 大牛版板所有
 */
public class Practice09 {
    public static void main(String[] args) {
        int a,b,c,max,middle,min;
        Scanner input = new Scanner(System.in);
        System.out.print("请输入三个整数：");
        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();
        max = a;
        if (b>a && b>c){
            max = b;
        }
        if (c>a && c>b){
            max = c;
        }
        min = a;
        if (b<a && b<c){
            min = b;
        }
        if (c<a && c<b){
            min = c;
        }
        middle = (a+b+c)-max-min;
        System.out.println("三个整数从小到大的排序为："+min+","+middle+","+max);
    }
}
